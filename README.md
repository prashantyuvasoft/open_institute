# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version
  * 2.4.1
* Rails version
  * 5.1.6

* Database creation
  * PostgreSQL
  * Development - open_institute_development
  * Test - open_institute_test
  * Production - open_institute_production

* Database initialization
  * rails db:create
  * rails db:migrate

* How to run the test suite
  * rspec

* ===== Workflow =====
  * AdminUser - Path - '/admin'
    * Email - 'admin@openinstitute.com'
    * Password - 'Faculty@123'    
    * AdminUser can create User [Faculty, Student]
    * Can re-assign Student to another Faculty anytime.
    * Can see Question(created by Faculties)
    * Can see Assignments(created by Students on 'Take Test')
    * Can see Results(calculated on Assignments submission)

  * Devise - User - Model
    * User has been used as 2 Inherited models
      * Faculty
      * Student

  * Faculty - Path - '/'
    * Password - 'Faculty@123'
    * Index page - List of Students (assigned to that Faculty by AdminUser) with there score details
    * Questions page - List of Question
    * New Question - Add New Question (MCQ with only one correct answer)
    * Edit Question(edit symbol inlast column) - Edit that question
    * Can create desired no. of questions.
    * Don't have any control on Students (Only admin can access modifications).

  * Student - PPath - '/'
    * Password - 'Student@123'
    * Index page - List of Assignments (test taken in latest first order with score details)
    * Take Test page - Going on this page adds new assignment to Student Assignments.

      * Assignment
        * It will contain max 10 questions (assuming one point for each)
        * Question will be selected automatically from Question of Faculty to whom this Student belongs
        * Once assigment page has been loaded, it cant be cancled.
        * Reloading that page again (after or before submitting current assignment) will cause to create one new assignment.
        * That's how we can put little session kind security.
        * On submission of assignment calculation will be done for Assignment and for Result as well.
        * Student can't re-take or edit assignment one it's unloaded fron 'Take test' page.

  * Pagination
    * All pages with table (list) will be using pagination of 10 records per page (can't be increased or decreased for now)

  * Rspec
    * Simple Specs has been written for followings:
      * Models
        * Faculty
        * Student
        * Question
        * Assignment
        * Result
      * Controllers
        * FacultyController
        * StudentController
