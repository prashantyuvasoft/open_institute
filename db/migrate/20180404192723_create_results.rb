class CreateResults < ActiveRecord::Migration[5.1]
  def change
    create_table :results do |t|
      t.references :student, index: true
      t.references :faculty#, index: true
      t.float :score, default: 0.0
      t.float :out_of, default: 0.0
      t.float :percentage, default: 0.0

      t.timestamps
    end
  end
end
