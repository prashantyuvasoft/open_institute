class CreateAssignments < ActiveRecord::Migration[5.1]
  def change
    create_table :assignments do |t|
      t.references :student, index: true
      t.json :answer_data, default: []
      t.boolean :completed, default: false
      t.float :score, default: 0.0
      t.float :percentage, default: 0.0

      t.timestamps
    end
  end
end
