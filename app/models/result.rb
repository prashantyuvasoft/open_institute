class Result < ApplicationRecord
	audited
  belongs_to :student
  belongs_to :faculty
end
