class Student < User
	audited
	belongs_to :faculty
  has_many :results, dependent: :destroy
  has_many :assignments, dependent: :destroy
	validates :faculty_id, presence: true, numericality: { only_integer:true, greater_than_or_equal_to:1 }

  before_validation :fill_password, if: :new_record?
  def fill_password
    self.password = "Student@123"
  end

  def name
    "#{first_name} #{last_name}"
  end

  def form_assignment
    assignment = self.assignments.new
    questions = self.faculty.questions.shuffle.first(10)
    if questions.present?
      assignment.answer_data = questions.map{|question| {que_id: question.id, answer: nil}}
      assignment.save!
      return questions, assignment, false
    else
      return [], nil, "Opps! No questions for assignment."
    end
  end
	
end