class Faculty < User
  audited
  has_many :students, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :results, dependent: :destroy

  before_validation :fill_password, if: :new_record?
  def fill_password
    self.password = "Faculty@123"
  end

  def name
    "#{first_name} #{last_name}"
  end
end