class Question < ApplicationRecord
	audited
  belongs_to :faculty
	validates :question, :answer_a, :answer_b, :answer_c, :answer_d, :right_answer, presence: true
end
