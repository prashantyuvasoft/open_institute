class Audited::Audit
	default_scope { where('user_type != ? AND user_id != ?', '', 0) }
  before_create :check
  def check
    self.auditable_type = self.auditable.type if self.auditable.user?
    self.user_type = self.user.type if self.user
    self.comment = "#{self.user_type}: #{self.user.name rescue ''}(ID: #{self.user_id}) #{(self.auditable.user? ? 'Signed In' : (self.action.capitalize+'d '+self.auditable_type+'(ID:'+self.auditable_id.to_s+')'))}"
  end
end
class Log < Audited::Audit
end
