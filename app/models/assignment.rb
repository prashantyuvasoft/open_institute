class Assignment < ApplicationRecord
  audited
  belongs_to :student

  # json :answer_data, default: []
  # boolean :completed, default: false
  # float :score, default: 0.0
  # float :percentage, default: 0.0

  before_update :update_result
  def update_result
    @result = Result.find_or_create_by(student_id: self.student_id, faculty_id: self.student.faculty_id)
    questions = Question.where(id: self.answer_data.map{|ans_d| ans_d['que_id']})
    score = self.answer_data.inject(0){|score, ans_d| score+(questions.find{|ques| ques.id==ans_d['que_id']}.right_answer==ans_d['answer'] ? 1.0 : 0.0) rescue 0.0}
    self.score = score
    @result.score = score
    self.percentage = (score*100)/self.answer_data.count
    @result.percentage = (score*100)/self.answer_data.count
    @result.out_of = self.answer_data.count
    @result.save!
  end
end
