module QuickActions
  extend ActiveSupport::Concern

  def user?
    self.class.superclass.name == 'User'
  end
end