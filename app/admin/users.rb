# ActiveAdmin.register User do
#   # menu parent: 'Persons'
#   permit_params :type, :first_name, :last_name, :date_of_birth, :email

#   # index do
#   #   selectable_column
#   #   id_column
#   #   column :first_name
#   #   column :last_name
#   #   column :date_of_birth
#   #   column :email
#   #   actions
#   # end

#   form do |f|
#     f.inputs "#{params[:action].capitalize} User" do
#       f.input :type, as: :select, collection: [['Faculty'], ['Student']], :input_html => {:required => true}
#       f.input :first_name
#       f.input :last_name
#       f.input :date_of_birth, :as => :date_picker
#       f.input :email
#     end
#     f.actions
#   end
# end
