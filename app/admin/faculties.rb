ActiveAdmin.register Faculty do
  menu parent: 'Users'
  permit_params :first_name, :last_name, :date_of_birth, :email

  filter :first_name_or_last_name_cont, label: 'Name'
  filter :email

  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :date_of_birth
    column :email
    actions defaults: true do |faculty|
      link_to "Logs", admin_logs_path(q: {user_type_eq: 'Faculty', user_id_eq: faculty.id}), class: "member_link"
    end
  end

  form do |f|
    f.inputs "#{params[:action].capitalize} Faculty" do
      # f.input :type, as: :select, collection: [['Faculty'], ['Student']], :input_html => {:required => true}
      f.input :first_name
      f.input :last_name
      f.input :date_of_birth, :as => :date_picker
      f.input :email
    end
    f.actions
  end
end
