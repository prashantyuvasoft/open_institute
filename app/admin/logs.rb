ActiveAdmin.register Log do
  actions :all, except: [:edit, :create, :new, :update]

  filter :user_type
  filter :user_id_eq, as: :string, label: 'User Id'
  filter :auditable_type
  filter :auditable_id_eq, as: :string, label: 'Auditable Id'

  index do
    selectable_column
    id_column
    column :user
    column :auditable
    column :created_at
    column :remote_address
    column :comment
    actions
  end
end
