ActiveAdmin.register Student do
  menu parent: 'Users'
  permit_params :first_name, :last_name, :date_of_birth, :email, :faculty_id

  filter :first_name_or_last_name_cont, label: 'Name'
  filter :email

  index do
    selectable_column
    id_column
    column :name
    column :date_of_birth
    column :email
    column :faculty
    actions defaults: true do |student|
      link_to "Logs", admin_logs_path(q: {user_type_eq: 'Student', user_id_eq: student.id}), class: "member_link"
    end
  end

  form do |f|
    f.inputs "#{params[:action].capitalize} Student" do
      # f.input :type, as: :select, collection: [['Faculty'], ['Student']], :input_html => {:required => true}
      f.input :first_name
      f.input :last_name
      f.input :date_of_birth, :as => :date_picker
      f.input :email
      f.input :faculty_id, as: :select, collection: Faculty.all.map{|fac| [fac.name, fac.id]}
    end
    f.actions
  end

  action_item only: [:show] do
    link_to 'Logs', admin_logs_path(q: {user_type_eq: 'Student', user_id_eq: student.id})
  end
end
