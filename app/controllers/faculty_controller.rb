class FacultyController < ApplicationController
  before_action :kick_out_student
  def index
    @students = current_user.students.order('created_at').page(params[:page]).per(10)
  end

  def questions
    @question = current_user.questions.new
    @questions = current_user.questions.page(params[:page]).per(10)
  end

  def new_question
    if request.get?
      @question = current_user.questions.new
    elsif request.post?
      @question = current_user.questions.new(question_params)
      @question.save!
    end
  end

  def edit_question
    begin
      params.require(:question_id)
      if request.xhr?
        @question = current_user.questions.find_by_id(params[:question_id])
        @action_name = params[:action]
        if request.put?
          @question.update(question_params)
          @action_name = "new_question"
        end
      end
    rescue => e
      @error = true
      @msg = e.message
    end
  end

  private
  def question_params
    params.require(:question).permit(:question, :answer_a, :answer_b, :answer_c, :answer_d, :right_answer)
  end

  def kick_out_student
    redirect_to student_root_path if current_user.type=='Student'
  end
end
