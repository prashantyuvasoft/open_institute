class ApplicationController < ActionController::Base
  before_action :authenticate_user!, unless: :devise_controller?
  protect_from_forgery with: :exception
  
  include ApplicationHelper  
  before_action :set_initial_variables

  def index  	
  end
end
