class StudentController < ApplicationController
  before_action :redirect_out_faculty

  def index
    @assignments = current_user.assignments.order('created_at desc').page(params[:page]).per(10)
  end

  def attempt
    @questions, @assignment, @error = current_user.form_assignment
    # debugger
  end

  def results
    
  end

  def process_attempt
    begin
      assignment_asnwers = params.require(:assignment).require(:answer_data)
      @assignment = current_user.assignments.find_by_id(params[:attempt_id])
      answer_data = @assignment.answer_data
      assignment_asnwers.each do |que_id, answer|
        answer_data.find{|ans_d| ans_d['que_id'] == que_id.to_i}['answer'] = answer
      end
      @assignment.completed = true
      if @assignment.save!
        raise "Success: Saved!"
      else
        raise "Error: #{@assignment.errors.full_messages.join(' ')}"
      end
    rescue => e
      @error = e.message
    end
    render :attempt
  end

  private
  def redirect_out_faculty
    redirect_to faculty_root_path if current_user.type=='Faculty'
  end
end
