Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users

  resources :faculty do
    get 'questions', on: :collection
    match 'new_question', on: :collection, via: [:get, :post]
    match 'edit_question/:question_id', as: :edit_question, action: :edit_question, on: :collection, via: [:get, :put]
  end
  resources :student do
    get 'attempt', on: :collection
    get 'results', on: :collection
    post ':attempt_id/process_attempt', on: :collection, action: :process_attempt, as: :process_attempt
  end

  authenticated :user, lambda { |u| u.type=="Faculty" } do
    root :to => 'faculty#index', :as => :faculty_root
  end

  authenticated :user, lambda { |u| u.type=="Student" } do
    root :to => 'student#index', :as => :student_root
  end

  root :to => 'application#index'
end
