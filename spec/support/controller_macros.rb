module ControllerMacros
  def login_admin
    before(:each) do
      @request.env["devise.mapping"] = Devise.mappings[:admin_user]
      sign_in FactoryBot.create(:admin_user) # Using factory girl as an example
    end
  end

  def login_faculty
    @request.env["devise.mapping"] = Devise.mappings[:user]
    faculty = faculty_find_or_create
    sign_in faculty
  end

  def login_student
    @request.env["devise.mapping"] = Devise.mappings[:user]
    student = student_find_or_create
    sign_in student
  end

  def faculty_find_or_create
    Faculty.find_or_create_by(
      first_name: "First",
      last_name: "Second",
      email: "faculty@openinstitute.com",
      date_of_birth: Date.today
    )
  end

  def student_find_or_create
    Student.find_or_create_by(
      first_name: "First",
      last_name: "Second",
      email: "student@openinstitute.com",
      date_of_birth: Date.today,
      faculty_id: (@current_user || faculty_find_or_create).id
    )
  end

  def question_find_or_create
    Question.find_or_create_by(
      question: "Question String",
      right_answer: "right_answer",
      answer_a: "answer_a",
      answer_b: "answer_b",
      answer_c: "answer_c",
      answer_d: "answer_d",
      faculty_id: (@current_user || faculty_find_or_create).id
    )
  end

  def result_find_or_create
    student = (@current_user || student_find_or_create)
    Result.new(
      student_id: student.id,
      faculty_id: student.faculty_id,
      score: 10.0,
      percentage: 100.0,
      out_of: 10.0
    )
  end

  def assignment_find_or_create
    student = (@current_user || student_find_or_create)
    Assignment.new(
      student_id: student.id,
      score: 0.0,
      percentage: 0.0,
      answer_data: []
    )
  end
  
  def find_list resource, count, current_user
    @current_user = current_user
    (1..count).map{|cc| eval(resource)}
  end
end