require 'rails_helper'

RSpec.describe Question, type: :model do
  it "is valid if all params are present" do
    expect(question_find_or_create).to be_valid
  end
  it "is not valid if question is nil" do
    question = Question.new(question: nil)
    expect(question).to_not be_valid
  end
  it "is not valid if right_answer is nil" do
    question = Question.new(right_answer: nil)
    expect(question).to_not be_valid
  end
  it "is not valid if answer_a is nil" do
    question = Question.new(answer_a: nil)
    expect(question).to_not be_valid
  end
  it "is not valid if answer_b is nil" do
    question = Question.new(answer_b: nil)
    expect(question).to_not be_valid
  end
  it "is not valid if answer_c is nil" do
    question = Question.new(answer_c: nil)
    expect(question).to_not be_valid
  end
  it "is not valid if answer_d is nil" do
    question = Question.new(answer_d: nil)
    expect(question).to_not be_valid
  end
end
