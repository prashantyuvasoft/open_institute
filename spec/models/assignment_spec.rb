require 'rails_helper'

RSpec.describe Assignment, type: :model do
  it "is valid if following params are present" do
    expect(assignment_find_or_create).to be_valid
  end
  it "is not valid if score is nil" do
    auction = Assignment.new(score: nil)
    expect(auction).to_not be_valid
  end
  it "is not valid if percentage is nil" do
    auction = Assignment.new(percentage: nil)
    expect(auction).to_not be_valid
  end
  it "is not valid if answer_data is nil" do
    auction = Assignment.new(answer_data: nil)
    expect(auction).to_not be_valid
  end
end
