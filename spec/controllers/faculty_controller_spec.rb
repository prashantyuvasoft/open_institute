require 'rails_helper'

RSpec.describe FacultyController, type: :controller do
  before(:each) do
    login_faculty
  end

  it "should have a current_user" do
    expect(subject.current_user).to_not eq(nil)
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      @students = find_list('student_find_or_create', 10, subject.current_user)
      expect(@students.size).to eq(10)
      render_template("index")
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #questions" do
    it "GET questions" do
      get :questions, params: {page: 1}
      @questions = find_list('question_find_or_create', 10, subject.current_user)
      expect(@questions.size).to eq(10)
      render_template("questions")
    end
  end

  describe "GET|POST #new_question" do
    it "GET new_question" do
      get :new_question, xhr: true
      @question = subject.current_user.questions.new
      render_template("new_question")
    end
    it "POST new_question" do
      post :new_question, xhr: true, params: {question: {
        question: "Question String",
        right_answer: "right_answer",
        answer_a: "answer_a",
        answer_b: "answer_b",
        answer_c: "answer_c",
        answer_d: "answer_d"
      }}
      @question = subject.current_user.questions.find_or_create_by(subject.params.require(:question).permit!)
      render_template("new_question")
    end
  end

  describe "GET|PUT #edit_question" do
    it "GET edit_question" do
      get :edit_question, xhr: true, params: {question_id: question_find_or_create.id}
      @question = Question.find_by_id(subject.params.require(:question_id))
      render_template("edit_question")
    end
    it "PUT edit_question" do
      post :edit_question, xhr: true, params: {question_id: question_find_or_create.id, question: {
        question: "Question String",
        right_answer: "right_answer",
        answer_a: "answer_a",
        answer_b: "answer_b",
        answer_c: "answer_c",
        answer_d: "answer_d"
      }}
      @question = subject.current_user.questions.find_by_id(subject.params.require(:question_id)).update(subject.params.require(:question).permit!)
      expect(@question).to render_template("edit_question")
    end
  end
end
