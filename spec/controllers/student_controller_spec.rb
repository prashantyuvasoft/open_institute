require 'rails_helper'

RSpec.describe StudentController, type: :controller do
  before(:each) do
    login_student
  end

  it "should have a current_user" do
    expect(subject.current_user).to_not eq(nil)
  end

  describe "GET #index" do
    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end
  end



  # def index
  #   @assignments = current_user.assignments.order('created_at desc').page(params[:page]).per(10)
  # end

  # def attempt
  #   @questions, @assignment, @error = current_user.form_assignment
  #   # debugger
  # end

  # def results
    
  # end

  # def process_attempt
  #   begin
  #     assignment_asnwers = params.require(:assignment).require(:answer_data)
  #     @assignment = current_user.assignments.find_by_id(params[:attempt_id])
  #     answer_data = @assignment.answer_data
  #     assignment_asnwers.each do |que_id, answer|
  #       answer_data.find{|ans_d| ans_d['que_id'] == que_id.to_i}['answer'] = answer
  #     end
  #     @assignment.completed = true
  #     if @assignment.save!
  #       raise "Success: Saved!"
  #     else
  #       raise "Error: #{@assignment.errors.full_messages.join(' ')}"
  #     end
  #   rescue => e
  #     @error = e.message
  #   end
  #   render :attempt
  # end

end
